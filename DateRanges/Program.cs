﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateRanges
{
    enum AvailabilityStatus { Available, Unavailable }

    class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AvailabilityStatus Status { get; set; }
    }

    class Program
    {
        public static List<DateTime> Unavailable = new List<DateTime> 
        {
            new DateTime(2017, 1, 3),
            new DateTime(2017, 1, 4),
            new DateTime(2017, 1, 5),
            new DateTime(2017, 1, 6),
            new DateTime(2017, 1, 27),
            new DateTime(2017, 1, 28),
            new DateTime(2017, 1, 29),
            new DateTime(2017, 1, 30)
        };

        public static List<DateRange> CreateUnavailableRanges(List<DateTime> dates)
        {
            List<DateTime> sortedDates = dates.OrderBy(x => x.Ticks).ToList();
            List<DateRange> result = new List<DateRange>();
            while (sortedDates.Any())
            {
                List<DateTime> item = sortedDates.TakeWhile((x, i) => i == 0 || (x - sortedDates[i - 1]).Days == 1).ToList();
                sortedDates = sortedDates.Skip(item.Count).ToList();
                result.Add(new DateRange { StartDate = item.First(), EndDate = item.Last(), Status = AvailabilityStatus.Unavailable });
            }
            return result;
        }

        public static List<DateRange> CreateAvailableRanges(List<DateRange> unanvailableRanges, DateTime start, DateTime end)
        {
            List<DateRange> availableRanges = new List<DateRange>();

            DateTime startOfNewRange = start;
            foreach (DateRange range in unanvailableRanges)
            {
                if (startOfNewRange < range.StartDate)
                {
                    availableRanges.Add(new DateRange() { StartDate = startOfNewRange, EndDate = range.StartDate.AddDays(-1), Status = AvailabilityStatus.Available });
                }
                startOfNewRange = range.EndDate.AddDays(1);
            }
            if (startOfNewRange <= end)
            {
                availableRanges.Add(new DateRange() { StartDate = startOfNewRange, EndDate = end, Status = AvailabilityStatus.Available });
            }
            return availableRanges;
        }

        public static List<DateRange> GetAvailabilityInfo(List<DateTime> dates, DateTime start, DateTime end)
        {
            //would be nice to cut the list from the end, but don't know how without reversing
            List<DateTime> unavailableDates = dates.SkipWhile(x => x < start).TakeWhile(x => x <= end).ToList();
            List<DateRange> unavailableRanges = CreateUnavailableRanges(unavailableDates);
            List<DateRange> availableRanges = CreateAvailableRanges(unavailableRanges, start, end);
            return unavailableRanges.Union(availableRanges).OrderBy(x => x.StartDate).ToList();
        }

        static void Main(string[] args)
        {
            foreach (DateRange range in GetAvailabilityInfo(Unavailable, new DateTime(2015, 1, 1), new DateTime(2018, 1, 31)))
            {
                Console.WriteLine(String.Format("{0:dd/MM/yyyy}-{1:dd/MM/yyyy} : {2}", range.StartDate, range.EndDate, range.Status.ToString()));
            }
            Console.ReadLine();
        }
    }
}
